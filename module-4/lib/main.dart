import 'package:flutter/material.dart';
import 'screens/Login.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

void main() {
  appStart(const theApp());
}

class theApp extends StatelessWidget {
  const theApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: const  SplashScreen(),
    );
    
  }
}
class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: Column (
          children: [
            Image.asset('lib/assets/IMG22.JPG'),
          ],
        ),
        splashIconSize: 5000,
        duration: 1000,
        backgroundColor: Colors.black,
        nextScreen: const LogInScreen()
      );
  }
}
